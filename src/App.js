import './styles/App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import Home from './routes/Home';
import Projects from './routes/Projects';
import Experience from './routes/Experience';
import Navigationbar from './components/Navigationbar';
import Footer from './components/Footer';
import ProjectDisplay from './routes/ProjectDisplay';

function App() {
  return (
    <div className="App">
      <Router>
        <Navigationbar/>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/projects" element={<Projects />} />
          <Route path="/project/:id" element={<ProjectDisplay />} />
          <Route path="/experience" element={<Experience />} />
        </Routes>
      <Footer/>
      </Router>
    </div>
  );
}

export default App;
