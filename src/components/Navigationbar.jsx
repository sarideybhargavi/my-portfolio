import React, { useEffect, useState} from 'react';
import { Link, useLocation } from 'react-router-dom';
import '../styles/NavigationBar.css';
import { Reorder } from '@material-ui/icons';

function NavigationBar() {
  const [expandNavBar, setExpandNavBar] = useState(false);
  const toggleExpandNavBar = () => setExpandNavBar(!expandNavBar);

  const location = useLocation();

  useEffect(() => {
    setExpandNavBar(false);
  }, [location]);

  return (
    <div className="navbar" id={expandNavBar ? "open" : "close"}>
      <div className="toggleButton">
        <button onClick={toggleExpandNavBar}>
          <Reorder/>
        </button>
      </div>
      <div className="links">
        <Link to="/">Home</Link>
        <Link to="/projects">Projects</Link>
        <Link to="/experience">Experience</Link>
      </div>
    </div>
  )
}

export default NavigationBar;