import PwdStrength from '../assets/pwd-strength.jpeg';
import Portfolio from '../assets/portfolio.webp';

export const projectsList = [
  {
    name: 'Password Strength Calculator',
    image: PwdStrength,
    skills: "HTML,ReactJS"
  },
  {
    name: 'My Portfolio',
    image: Portfolio,
    skills: "HTML,CSS,ReactJS"
  },
];