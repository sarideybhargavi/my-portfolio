import React from 'react'
import { VerticalTimeline, VerticalTimelineElement } from 'react-vertical-timeline-component'
import "react-vertical-timeline-component/style.min.css";
import SchoolIcon from '@material-ui/icons/School';
import WorkIcon from '@material-ui/icons/Work';
import MenuBookIcon from '@material-ui/icons/MenuBook';
import { FlightTakeoff } from '@material-ui/icons';

function Experience() {
  return (
    <div className="experience">
      <VerticalTimeline lineColor="#3e497a">
      <VerticalTimelineElement
          className="vertical-timeline-element--education"
          date="Oct 2010 - May 2014"
          iconStyle={{ background: "#3e497a", color: "#fff" }}
          icon={<SchoolIcon />}
        >
          <h3 className="vertical-timeline-element-title">Jawaharlal Nehru TU, Kakinada, India</h3>
          <p>Bachelors in Technology, Computer Science and Engineering</p>
        </VerticalTimelineElement>
        <VerticalTimelineElement
          className="vertical-timeline-element--work"
          date="Jun 2014 - Dec 2015"
          iconStyle={{ background: "#e9d35b", color: "#fff" }}
          icon={<MenuBookIcon />}
        >
          <h3 className="vertical-timeline-element-title">Professional Development</h3>
          <p>Career path change and prepping for exams</p>
        </VerticalTimelineElement>
        <VerticalTimelineElement
          className="vertical-timeline-element--work"
          date="Jan 2016 - May 2023"
          iconStyle={{ background: "#088F8F", color: "#fff" }}
          icon={<WorkIcon />}
        >
          <h3 className="vertical-timeline-element-title">State Bank of India</h3>
          <p>Associate(Customer Support and Sales)</p>
        </VerticalTimelineElement>
        <VerticalTimelineElement
          className="vertical-timeline-element--work"
          date="Jun 2023"
          iconStyle={{ background: "#FF5733", color: "#fff" }}
          icon={<FlightTakeoff />}
        >
          <h3 className="vertical-timeline-element-title">Moved to Canada from India</h3>
          <p>Moved with my Partner</p>
        </VerticalTimelineElement>
      </VerticalTimeline>
    </div>
  )
}

export default Experience