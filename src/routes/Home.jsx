import React from 'react'
import LinkedIn from '@material-ui/icons/LinkedIn'
import Github from '@material-ui/icons/GitHub'
import { Email } from '@material-ui/icons'
import '../styles/Home.css'

function Home() {
  return (
    <div className="home">
      <div className="about">
        <h2>Hello, My name is Bhargavi</h2>
        <div className="prompt">
          <p>
            A Software enthusiast with a passion for learning and creating
          </p>
          <a href="https://www.linkedin.com/in/bhargavi-saridey-6685252b2/" target="_blank"><LinkedIn/></a>
          <Email/>
          <a href="https://gitlab.com/sarideybhargavi" target="_blank"><Github/></a>
        </div>
      </div>
      <div className="skills">
        <h1>Skills</h1>
        <ol className="list">
          <li className="item">
            <h2>Frontend</h2>
            <span> HTML,CSS,ReactJS</span>
          </li>
          <li className="item">
            <h2>Languages</h2>
            <span> JavaScript,TypeScript</span>
            </li>
            <li className="item">
            <h2>Others</h2>
            <span> Git,Gitlab</span>
          </li>
        </ol>
      </div>
    </div>
  )
}

export default Home