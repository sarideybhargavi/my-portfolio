import React from 'react'
import ProjectItem from '../components/ProjectItem';
import { projectsList } from '../helpers/ProjectList';
import '../styles/Projects.css'

function Projects() {
  return (
    <div className="projects">
      <h1>My Personal Projects</h1>
      <div className="projectList">
        {
          projectsList.map((project, index) => (<ProjectItem name={project.name} image={project.image} id={index}/>))
        }
      </div>
    </div>
  )
}

export default Projects